package signals

import "context"

// Slot interface
type Slot interface {
	OnSignal(ctx context.Context) error
}

// NewSlot create slot
func NewSlot(f func(ctx context.Context) error) Slot {
	return slot{
		f: f,
	}
}

type slot struct {
	f func(ctx context.Context) error
}

func (s slot) OnSignal(ctx context.Context) error {
	if s.f == nil {
		return nil
	}
	return s.f(ctx)
}
