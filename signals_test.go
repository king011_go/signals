package signals_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/king011_go/signals"
	"gitlab.com/king011_go/signals/metadata"
)

func TestSignals(t *testing.T) {
	s := signals.Signals{}

	s.Connect(signals.NewSlot(func(ctx context.Context) error {
		md, ok := metadata.FromContext(ctx)
		assert.True(t, ok)
		left := md.Assert(`left`).(int)
		assert.Equal(t, left, 1)
		right := md.Assert(`right`).(int)
		assert.Equal(t, right, 2)
		md.Set(`result`, left+right)
		return nil
	}))
	ctx := metadata.NewContext(context.Background(), metadata.Pairs(`left`, 1, `right`, 2))
	e := s.Emit(ctx)
	assert.Nil(t, e)
	md, ok := metadata.FromContext(ctx)
	assert.True(t, ok)
	result := md.Assert(`result`).(int)
	assert.Equal(t, result, 3)

	md.Delete(`left`)
	assert.Equal(t, md.Len(), 2)
	md.Clear()
	assert.Equal(t, md.Len(), 0)
}
