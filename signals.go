package signals

import "context"

// Connection .
type Connection struct {
	noCopy
	signals *Signals
	slot    Slot
	events  []uint
}

// Disconnect slot
func (c *Connection) Disconnect() (ok bool) {
	return c.signals.Disconnect(c)
}

// Signals and slots
type Signals struct {
	noCopy
	connections []*Connection
}

// Connect slot
func (s *Signals) Connect(slot Slot, events ...uint) *Connection {
	connection := &Connection{
		slot:   slot,
		events: events,
	}
	s.connections = append(s.connections, connection)
	return connection
}

// Len return connected slot count
func (s *Signals) Len() int {
	return len(s.connections)
}

// Disconnect slot
func (s *Signals) Disconnect(connection *Connection) (ok bool) {
	for i, item := range s.connections {
		if item == connection {
			ok = true
			left := s.connections[0:i]
			right := s.connections[i+1:]
			s.connections = left
			s.connections = append(s.connections, right...)
			break
		}
	}
	return
}

// Clear slot
func (s *Signals) Clear(slots ...Slot) {
	if len(s.connections) != 0 {
		s.connections = s.connections[:0]
	}
}

// Emit a signal
func (s *Signals) Emit(ctx context.Context, events ...uint) (e error) {
	for _, connection := range s.connections {
		if len(events) == 0 { // anonymous signal

			if len(connection.events) != 0 { // care name signal
				continue
			}
		} else { // name signal
			if len(connection.events) != 0 { // care name signal
				if !s.match(events, connection.events) {
					continue
				}
			}
		}

		// call slot
		e = connection.slot.OnSignal(ctx)
		if e != nil {
			return
		}
	}
	return
}
func (s *Signals) match(left, right []uint) bool {
	for _, l := range left {
		for _, r := range right {
			if l == r {
				return true
			}
		}
	}
	return false
}
