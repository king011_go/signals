package metadata

import (
	"context"
	"fmt"
)

// MD is a mapping from metadata keys to values. Users should use the following
// two convenience functions NewMD and PairsMD to generate MD.
type MD map[interface{}]interface{}

// New creates an MD from a given key-value map.
func New(m map[interface{}]interface{}) MD {
	md := MD{}
	for k, val := range m {
		md[k] = val
	}
	return md
}

// Pairs returns an MD formed by the mapping of key, value ...
// Pairs panics if len(kv) is odd.
func Pairs(kv ...interface{}) MD {
	if len(kv)%2 == 1 {
		panic(fmt.Sprintf("metadata: Pairs got the odd number of input pairs for metadata: %d", len(kv)))
	}
	md := MD{}
	var key interface{}
	for i, s := range kv {
		if i%2 == 0 {
			key = s
			continue
		}
		md[key] = s
	}
	return md
}

// Len returns the number of items in md.
func (md MD) Len() int {
	return len(md)
}

// Copy returns a copy of md.
func (md MD) Copy() MD {
	return Join(md)
}

// Get obtains the values for a given key.
func (md MD) Get(k interface{}) (v interface{}, exists bool) {
	v, exists = md[k]
	return
}

// Assert obtains the values for a given key.
func (md MD) Assert(k interface{}) interface{} {
	return md[k]
}

// Set sets the value of a given key with a slice of value.
func (md MD) Set(k interface{}, val interface{}) {
	md[k] = val
}

// Clear keys
func (md MD) Clear() {
	for k := range md {
		delete(md, k)
	}
}

// Delete key
func (md MD) Delete(k interface{}) {
	delete(md, k)
}

// Join joins any number of mds into a single MD.
// The order of values for each key is determined by the order in which
// the mds containing those values are presented to Join.
func Join(mds ...MD) MD {
	out := MD{}
	for _, md := range mds {
		for k, v := range md {
			out[k] = v
		}
	}
	return out
}

type mdKey struct{}

// NewContext creates a new context with md attached.
func NewContext(ctx context.Context, md MD) context.Context {
	return context.WithValue(ctx, mdKey{}, md)
}

// FromContext returns the incoming metadata in ctx if it exists.
func FromContext(ctx context.Context) (md MD, ok bool) {
	md, ok = ctx.Value(mdKey{}).(MD)
	return
}
